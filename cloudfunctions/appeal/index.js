// 云函数入口文件
const appealBrowseLog = require('./appealBrowseLog/index')
const appealDetail = require('./appealDetail/index')
const appealPutComment = require('./appealPutComment/index')
const appealSave = require('./appealSave/index')
const appealToCancel = require('./appealToCancel/index')
const appealToEndorse = require('./appealToEndorse/index')
const pageAppeal = require('./pageAppeal/index')
const pageAppealComment = require('./pageAppealComment/index')
const appealTag = require('./appealTag/index')
const cloud = require('wx-server-sdk')
cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {

  const api = event.api //接口类型
  const params = event.data //所传递的数据
  let exportData = {}
  //错误码
  const responseCode = await cloud.callFunction({
    name: 'code'
  })



  if (api === 'appealBrowseLog') { // 增加浏览记录

    const result = await appealBrowseLog(params)
    exportData = Object.assign({
      data: result
    }, result ? responseCode.result['00000'] : responseCode.result['00001'])


  } else if (api === 'appealDetail') { // 诉求详情

    const result = await appealDetail(params)
    exportData = Object.assign({
      data: result
    }, responseCode.result['00000'])

  } else if (api === 'appealPutComment') { // 保存诉求评论

    const result = await appealPutComment(params)
    exportData = Object.assign({
      data: result
    }, result ? responseCode.result['00000'] : responseCode.result['00001'])

  } else if (api === 'appealSave') { // 保存诉求

    const result = await appealSave(params)
    exportData = Object.assign({
      data: result
    }, result ? responseCode.result['00000'] : responseCode.result['00001'])

  } else if (api === 'appealToCancel') { // 取消诉求点赞

    const result = await appealToCancel(params)
    exportData = Object.assign({
      data: result
    }, result ? responseCode.result['00000'] : responseCode.result['00001'])

  } else if (api === 'appealToEndorse') { // 诉求点赞

    const result = await appealToEndorse(params)
    exportData = Object.assign({
      data: result
    }, result ? responseCode.result['00000'] : responseCode.result['00001'])

  } else if (api === 'pageAppeal') { // 分页查询诉求

    const result = await pageAppeal(params)
    exportData = Object.assign({
      data: result
    }, responseCode.result['00000'])

  } else if (api === 'pageAppealComment') { // 分页查询诉求评论

    const result = await pageAppealComment(params)
    exportData = Object.assign({
      data: result
    }, responseCode.result['00000'])

  } else if (api === 'appealTag') {

    const result = await appealTag(params)
    exportData = Object.assign({
      data: result
    }, responseCode.result['00000'])

  }


  return exportData

}